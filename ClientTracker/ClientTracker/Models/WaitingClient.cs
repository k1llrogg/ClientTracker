﻿using ClientTracker.Enums;
using ClientTracker.Interfaces;
using System;
using System.ComponentModel;

namespace ClientTracker.Models
{
	[Serializable]
	class WaitingClient : IClient
	{
		private string _nickname;
		private ClientDesiredSocialPlatform _desiredSocialPlatform;

		public WaitingClient(string nickname, ClientDesiredSocialPlatform desiredSocialPlatform)
		{
			_nickname = nickname;
			_desiredSocialPlatform = desiredSocialPlatform;
		}

		public string Nickname
		{
			get => _nickname;
			set
			{
				_nickname = value;
				NotifyOfPropertyChange("Nickname");
			}
		}

		public ClientDesiredSocialPlatform DesiredSocialPlatform
		{
			get => _desiredSocialPlatform;
			set
			{
				_desiredSocialPlatform = value;
				NotifyOfPropertyChange("DesiredSocialPlatform");
			}
		}

		[field: NonSerialized()]
		public event PropertyChangedEventHandler PropertyChanged;
		protected void NotifyOfPropertyChange(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
