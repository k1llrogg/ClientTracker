﻿using ClientTracker.Enums;
using ClientTracker.Interfaces;
using System;
using System.ComponentModel;
using System.Diagnostics;

namespace ClientTracker.Models
{
	[Serializable]
	class InProgressClient : IClient
	{
		private string _nickname;
		private DateTime?_waitUntil;
		private ClientDesiredSocialPlatform _clientDesiredSocialPlatform;

		public InProgressClient(string nickname, DateTime? waitUntil, ClientDesiredSocialPlatform clientDesiredSocialPlatform)
		{
			_nickname = nickname;
			_waitUntil = waitUntil;
			_clientDesiredSocialPlatform = clientDesiredSocialPlatform;
		}

		public string Nickname
		{
			get => _nickname;
			set
			{
				_nickname = value;
				NotifyOfPropertyChange("Nickname");
			}
		}

		public DateTime? WaitUntil
		{
			get => _waitUntil;
			set
			{
				_waitUntil = value;
				NotifyOfPropertyChange("WaitUntil");
			}
		}

		public ClientDesiredSocialPlatform DesiredSocialPlatform
		{
			get => _clientDesiredSocialPlatform;
			set
			{
				_clientDesiredSocialPlatform = value;
				NotifyOfPropertyChange("DesiredSocialPlatform");
			}
		}

		[field: NonSerialized()]
		private RelayCommand _verifyClientCommand;
		public RelayCommand VerifyClientCommand
		{
			get
			{
				return _verifyClientCommand ??
				       (_verifyClientCommand = new RelayCommand(obj =>
					       {
						       WaitUntil = null;
					       }));
			}
		}

		[field: NonSerialized()]
		private RelayCommand _openProfileCommand;
		public RelayCommand OpenProfileCommand
		{
			get
			{
				return _openProfileCommand ??
				       (_openProfileCommand = new RelayCommand(obj =>
					       {
							   Process.Start($"http://www.furaffinity.net/newpm/{Nickname}");
						   }));
			}
		}

		[field:NonSerialized()]
		public event PropertyChangedEventHandler PropertyChanged;
		protected void NotifyOfPropertyChange(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
