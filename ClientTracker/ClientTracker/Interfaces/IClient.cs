﻿using ClientTracker.Enums;
using System.ComponentModel;

namespace ClientTracker.Interfaces
{
	public interface IClient : INotifyPropertyChanged
	{
		string Nickname { get; set; }
		ClientDesiredSocialPlatform DesiredSocialPlatform { get; set; }
	}
}