﻿using ClientTracker.ViewModels;

namespace ClientTracker
{
	public partial class MainWindow
	{
		public MainWindow()
		{
			InitializeComponent();
			var clientsViewModel = new ClientsViewModel();
			DataContext = clientsViewModel;

			Closing += clientsViewModel.OnWindowClosing;
		}
	}
}
