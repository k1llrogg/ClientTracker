﻿using ClientTracker.Enums;
using ClientTracker.Helpers;
using ClientTracker.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Forms;

namespace ClientTracker.ViewModels
{
	class ClientsViewModel : INotifyPropertyChanged
	{
		private InProgressClient _selectedInProgressClient;
		private WaitingClient _selectedWaitingClient;
		private ObservableCollection<InProgressClient> _inProgressClients;
		private ObservableCollection<WaitingClient> _waitingClients;
		private RelayCommand _addClientCommand;
		private RelayCommand _deleteInProgressClientCommand;
		private RelayCommand _deleteWaitingClientCommand;
		private RelayCommand _backupDataCommand;
		private RelayCommand _importFromTextCommand;

		public InProgressClient SelectedInProgressClient
		{
			get => _selectedInProgressClient;
			set
			{
				_selectedInProgressClient = value;
				NotifyOfPropertyChange("SelectedInProgressClient");
			}
		}
		public WaitingClient SelectedWaitingClient
		{
			get => _selectedWaitingClient;
			set
			{
				_selectedWaitingClient = value;
				NotifyOfPropertyChange("SelectedWaitingClient");
			}
		}

		public ObservableCollection<InProgressClient> InProgressClients
		{
			get => _inProgressClients;
			set
			{
				_inProgressClients = value;
				NotifyOfPropertyChange("InProgressClients");
			}
		}
		public ObservableCollection<WaitingClient> WaitingClients
		{
			get => _waitingClients;
			set
			{
				_waitingClients = value;
				NotifyOfPropertyChange("WaitingClients");
			}
		}

		public RelayCommand AddClientCommand
		{
			get
			{
				return _addClientCommand ??
				       (_addClientCommand = new RelayCommand(obj =>
				       {
					       try
					       {
						       var client = new WaitingClient("Введи здесь", ClientDesiredSocialPlatform.Discord);
						       WaitingClients.Add(client);
						       SelectedWaitingClient = client;
						       UpdateCollections();
					       }
					       catch (Exception ex)
					       {
							   MessageBox.Show($"Ошибка: {ex.Message}.\nСкажи Эду!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
						   }
				       }));
			}
		}

		public RelayCommand DeleteInProgressClientCommand
		{
			get
			{
				return _deleteInProgressClientCommand ??
				       (_deleteInProgressClientCommand = new RelayCommand(obj =>
					       {
						       try
						       {
							       if (obj == null) return;
							       InProgressClients.Remove(SelectedInProgressClient);
							       if (InProgressClients.Count > 0) SelectedInProgressClient = InProgressClients[InProgressClients.Count - 1];
							       UpdateCollections();
						       }
						       catch (Exception ex)
						       {
								   MessageBox.Show($"Ошибка: {ex.Message}.\nСкажи Эду!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
							   }
						   },
					       (obj) => InProgressClients.Count > 0));
			}
		}

		public RelayCommand DeleteWaitingClientCommand
		{
			get
			{
				return _deleteWaitingClientCommand ??
				       (_deleteWaitingClientCommand = new RelayCommand(obj =>
					       {
						       try
						       {
							       if (obj == null) return;
							       WaitingClients.Remove(SelectedWaitingClient);
							       if (WaitingClients.Count > 0) SelectedWaitingClient = WaitingClients[WaitingClients.Count - 1];
							       UpdateCollections();
						       }
						       catch (Exception ex)
						       {
								   MessageBox.Show($"Ошибка: {ex.Message}.\nСкажи Эду!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
							   }
						   },
					       (obj) => WaitingClients.Count > 0));
			}
		}

		public RelayCommand BackupDataCommand
		{
			get
			{
				return _backupDataCommand ??
				       (_backupDataCommand = new RelayCommand(obj =>
					   {
						   try
						   {
							   BackupData();
							   MessageBox.Show("Done! :)\nФайл сохранен в data_backup.bin.\nЧтобы восстановить данные, переименуй его в data.bin", "Yay!", MessageBoxButtons.OK, MessageBoxIcon.Information);

						   }
						   catch (Exception ex)
						   {
							   MessageBox.Show($"Ошибка: {ex.Message}.\nСкажи Эду!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
						   }
					   }));
			}
		}

		public RelayCommand ImportFromTextCommand
		{
			get
			{
				return _importFromTextCommand ??
				       (_importFromTextCommand = new RelayCommand(obj =>
					       {
						       try
						       {
							       SerializableData data = TextParser.ParseText();
							       if (data == null) return;
							       InProgressClients = data.InProgressClients;
							       WaitingClients = data.WaitingClients;
						       }
						       catch (Exception ex)
						       {
									MessageBox.Show($"Ошибка: {ex.Message}.\nСкажи Эду!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
							   }
					       }));
			}
		}

		private void BackupData()
		{
			var data = new SerializableData(InProgressClients, WaitingClients);
			SerializationHelper.SerializeData(data, "data_backup.bin");
		}

		public ClientsViewModel()
		{
			var data = SerializationHelper.DeserializeData();
			InProgressClients = data.InProgressClients;
			WaitingClients = data.WaitingClients;
		}

		private void UpdateCollections()
		{
			if (InProgressClients.Count >= 6) return;
			for (int i = 0; i < 6 - InProgressClients.Count && i < WaitingClients.Count; i++)
			{
				var waitingClient = WaitingClients[i];
				var inProgressClient = new InProgressClient(waitingClient.Nickname, DateTime.Now.AddDays(7), waitingClient.DesiredSocialPlatform);

				WaitingClients.Remove(waitingClient);
				InProgressClients.Add(inProgressClient);

				var data = new SerializableData(InProgressClients, WaitingClients);
				SerializationHelper.SerializeData(data);
			}
		}

		public void OnWindowClosing(object sender, CancelEventArgs e)
		{
			var data = new SerializableData(InProgressClients, WaitingClients);
			SerializationHelper.SerializeData(data);
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void NotifyOfPropertyChange(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
