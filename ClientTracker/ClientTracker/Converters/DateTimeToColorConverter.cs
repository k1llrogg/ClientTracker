﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace ClientTracker.Converters
{
	[ValueConversion(typeof(DateTime?), typeof(Brushes))]
	class DateTimeToColorConverter : IValueConverter
    {
	    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
	    {
		    var date = value as DateTime?;
		    if (value == null || DateTime.Now.Date.AddDays(1) < date.Value.Date) return Brushes.Transparent;
		    if (DateTime.Now.Date.AddDays(1) == date.Value.Date) return Brushes.DarkGreen;
		    return Brushes.Red;
	    }

	    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
	    {
		    throw new NotImplementedException();
	    }
	}
}
