﻿using ClientTracker.Enums;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ClientTracker.Converters
{
	[ValueConversion(typeof(ClientDesiredSocialPlatform), typeof(Visibility))]
	class DesiredPlatformToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType,
			object parameter, CultureInfo culture)
		{
			if (value == null) return Visibility.Hidden;
			var enumString = Enum.GetName(value.GetType(), value);
			return enumString == "FurAffinity" ? Visibility.Visible : Visibility.Hidden;

		}

		public object ConvertBack(object value, Type targetType,
			object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
