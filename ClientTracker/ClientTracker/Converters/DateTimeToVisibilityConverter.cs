﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ClientTracker.Converters
{
	[ValueConversion(typeof(DateTime?), typeof(Visibility))]
	class DateTimeToVisibilityConverter : IValueConverter
    {
	    public object Convert(object value, Type targetType,
		    object parameter, CultureInfo culture)
	    {
		    return value == null ? Visibility.Hidden : Visibility.Visible;
	    }

	    public object ConvertBack(object value, Type targetType,
		    object parameter, CultureInfo culture)
	    {
		    throw new NotImplementedException();
	    }
	}
}
