﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ClientTracker.Enums;
using ClientTracker.Models;

namespace ClientTracker.Helpers
{
	static class TextParser
	{
		private static string[] _data;
		private static ObservableCollection<WaitingClient> _waitingClients;
		private static ObservableCollection<InProgressClient> _inProgressClients;
		public static SerializableData ParseText()
		{
			_waitingClients = new ObservableCollection<WaitingClient>();
			_inProgressClients = new ObservableCollection<InProgressClient>();
			var openFileDialog = new OpenFileDialog
			{
				Filter = "txt files (*.txt)|*.txt",
				RestoreDirectory = true
			};


			if (openFileDialog.ShowDialog() != DialogResult.OK) return null;
			try
			{
				var rawData = File.ReadAllText(openFileDialog.FileName);
				_data = rawData.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

				foreach(var row in _data)
				{
					NextRow(row);
				}
				return new SerializableData(_inProgressClients, _waitingClients);
			}
			catch (Exception ex)
			{
				MessageBox.Show($"Ошибка: {ex.Message}.\nСкажи Эду!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return null;
			}
		}

		private static void NextRow(string row)
		{
			if (Regex.IsMatch(row[0].ToString(), "^[1-9]$"))
			{
				AddToInProgressClients(row);
				return;
			}

			if (Regex.IsMatch(row[0].ToString(), "^[-]$"))
			{
				AddToWaitingClients(row);
			}
		}

		private static void AddToWaitingClients(string row)
		{
			string[] waitingClientData = row.Remove(0, 1).Trim().Split(' ');

			var platform = Regex.IsMatch(waitingClientData[0], "[#][0-9][0-9][0-9][0-9]") ? ClientDesiredSocialPlatform.Discord : ClientDesiredSocialPlatform.FurAffinity;

			var waitingClient = new WaitingClient(waitingClientData[0], platform);
			_waitingClients.Add(waitingClient);
		}

		private static void AddToInProgressClients(string row)
		{
			string[] inProgressClientData = row.Remove(0, 2).Trim().Split(new []{' '}, StringSplitOptions.RemoveEmptyEntries);

			var platform = Regex.IsMatch(inProgressClientData[0], "[#][0-9][0-9][0-9][0-9]") ? ClientDesiredSocialPlatform.Discord : ClientDesiredSocialPlatform.FurAffinity;
			var date = inProgressClientData.Length > 1
				? (DateTime?) new DateTime(DateTime.Now.Year, DateTime.Now.Month,
					Convert.ToInt32(inProgressClientData[1])).AddDays(7)
				: null;

			var inProgressClient = new InProgressClient(inProgressClientData[0], date, platform);
			_inProgressClients.Add(inProgressClient);
		}
	}
}	