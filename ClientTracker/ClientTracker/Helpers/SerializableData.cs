﻿using System;
using ClientTracker.Models;
using System.Collections.ObjectModel;

namespace ClientTracker.Helpers
{
	[Serializable]
	class SerializableData
	{
		public ObservableCollection<InProgressClient> InProgressClients { get; }
		public ObservableCollection<WaitingClient> WaitingClients { get; }

		public SerializableData(ObservableCollection<InProgressClient> inProgressClients, ObservableCollection<WaitingClient> waitingClients)
		{
			InProgressClients = inProgressClients;
			WaitingClients = waitingClients;
		}
	}
}
