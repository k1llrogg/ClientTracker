﻿using ClientTracker.Models;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ClientTracker.Helpers
{
	static class SerializationHelper
	{
		private static readonly BinaryFormatter Bf = new BinaryFormatter();

		public static void SerializeData(SerializableData data, string filename = "data.bin")
		{
			using (var fs = File.Create(filename))
			{
				Bf.Serialize(fs, data);
			}
		}

		public static SerializableData DeserializeData()
		{
			using (var fs = File.Open("data.bin", FileMode.OpenOrCreate))
			{
				if (fs.Length == 0)

					return new SerializableData(new ObservableCollection<InProgressClient>(),
						new ObservableCollection<WaitingClient>());

				return (SerializableData) Bf.Deserialize(fs);
			}
		}
	}
}